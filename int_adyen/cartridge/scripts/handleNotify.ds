/**
 * save Adyen Notification
 * see page 22 of Adyen Integration manual
 *
 * v1 110324 : logging to file
 * v2 110325 :
 * v3 110408 : pass on OrderNo, Paymentresult for update
 * v4 130422 : Merged adyen_notify and update_order into single script  
 *
 * @input CurrentHttpParameterMap : Object
 *
 * @output Order			: dw.order.Order	The updated order 
 * @output EventCode		: String 			The event code
 * @output SkipNotification	: Boolean 			Skip email notification
 *
 */
importPackage( dw.system );
importPackage( dw.util );
importPackage( dw.value);
importPackage( dw.order); 

function execute( args : PipelineDictionary ) : Number
{
	//configuration
	var msg : String;
	var hpm: Object = args.CurrentHttpParameterMap;
	
	var paymentSuccess = false;
	
	args.EventCode = hpm.eventCode.stringValue;
	args.SkipNotification = false;

	// log detailed response to ScriptLog and log file
	msg = createLogMessage(hpm);
	Logger.getLogger("Adyen").debug(msg);
	
	var order : Order = OrderMgr.getOrder(hpm.merchantReference.stringValue);
	args.Order = order;
	
	if(order == null){
		Logger.getLogger("Adyen").fatal("Notification for not existing order {0} received.",hpm.merchantReference.stringValue);  
	}
	
	// update the order status
	switch(hpm.eventCode.stringValue){
		case "AUTHORISATION" :
			if(hpm.success.stringValue == "true"){
				if(order.paymentStatus == Order.PAYMENT_STATUS_PAID){
					Logger.getLogger("Adyen").info ("Duplicate callback received for order {0}.",order.orderNo);
					args.SkipNotification = true;
				}
				else if(order.paymentStatus == "PARTIALLY PAID"){
					paymentSuccess = true;
					Logger.getLogger("Adyen").info ("Partial payment received for order {0}.",order.orderNo);
					args.SkipNotification = true;
				}
				else {
					paymentSuccess = true;
					order.setPaymentStatus(Order.PAYMENT_STATUS_PAID);
					order.setExportStatus(Order.EXPORT_STATUS_READY);
					Logger.getLogger("Adyen").info ("Order {0} updated to status PAID.",order.orderNo);
				}  
			}else{
				Logger.getLogger("Adyen").info ("Authorization for order {0} was not successful - no update.",order.orderNo);
			}
			break;
		case "CANCELLATION" :
			order.setPaymentStatus(Order.PAYMENT_STATUS_NOTPAID);
			order.setExportStatus(Order.EXPORT_STATUS_NOTEXPORTED);
			Logger.getLogger("Adyen").info ("Order {0} was cancelled.",order.orderNo);
			break;
		case "REFUND" :
			order.setPaymentStatus(Order.PAYMENT_STATUS_NOTPAID);
			order.setExportStatus(Order.EXPORT_STATUS_NOTEXPORTED);
			Logger.getLogger("Adyen").info ("Order {0} was refunded.",order.orderNo);
			break;
		case "ORDER_OPENED" :
			Logger.getLogger("Adyen").info ("Order {0} updated to status PARTIALLY PAID.",order.orderNo);  
			paymentSuccess = true;
			order.setPaymentStatus("PARTIALLY PAID");
			break;	
		case "ORDER_CLOSED" :
			paymentSuccess = true;
			order.setPaymentStatus(Order.PAYMENT_STATUS_PAID);
			order.setExportStatus(Order.EXPORT_STATUS_READY);
			Logger.getLogger("Adyen").info ("Order {0} closed and updated to status PAID.",order.orderNo);  
			break;					
			
		default:
			Logger.getLogger("Adyen").info ("Order {0} received unhandled status {1}",order.orderNo,hpm.eventCode.stringValue);
	}
	
	// add received information to order
	order.custom.Adyen_pspReference = hpm.pspReference.stringValue;
	order.custom.Adyen_value = hpm.value.stringValue;
	order.custom.Adyen_eventCode =hpm.eventCode.stringValue;
	order.custom.Adyen_paymentMethod = hpm.paymentMethod.stringValue;
	// add a note with all details
	order.addNote("Adyen Payment Notification",msg);
	
	if(!paymentSuccess){
		return PIPELET_ERROR;
	}

    return PIPELET_NEXT;
}

function createLogMessage(hpm){
	var VERSION : String= "4d";
	var msg = "";
	msg = "AdyenNotification v " + VERSION + " - Payment info (Called from : " + request.httpRemoteAddress + ")";
	msg = msg + "\n================================================================\n";
    //msg = msg + "\nSessionID : " + args.CurrentSession.sessionID;
    msg = msg + "reason : " + hpm.reason.stringValue;
	msg = msg + "\neventDate : " + hpm.eventDate.stringValue;
	msg = msg + "\nmerchantReference : " + hpm.merchantReference.stringValue; 
	msg = msg + "\ncurrency : " + hpm.currency.stringValue;
	msg = msg + "\npspReference : " + hpm.pspReference.stringValue;
	msg = msg + "\nmerchantAccountCode : " + hpm.merchantAccountCode.stringValue;
	msg = msg + "\neventCode : " + hpm.eventCode.stringValue;
	msg = msg + "\nvalue : " + hpm.value.stringValue;
	msg = msg + "\noperations : " + hpm.operations.stringValue;
	msg = msg + "\nsuccess : " + hpm.success.stringValue;
	msg = msg + "\npaymentMethod : " + hpm.paymentMethod.stringValue;
	msg = msg + "\nlive : " + hpm.live.stringValue;
	return msg;
}